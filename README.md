# ReadMe

## Folder Structure

* The main song-recommender folder contains the .gitignore, readme, requirements.txt files. Also includes the application and data folders.
* The application folder holds the main codebase. This includes the static folder, the templates folder, and files for forms, the ML model and routes. The static folder holds the style.css for front-end style, the Images folder and some font files. The templates folder holds the pages for the website.
* The data folder holds the dataset of songs
