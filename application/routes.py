from flask import *
import pandas as pd
from model import playlist_extractor, multiple_song_recommender
from flask_wtf.csrf import CSRFProtect
from forms import PlaylistForm
from PIL import Image
import os, os.path
import glob


app = Flask(__name__)
csrf = CSRFProtect(app)
app.config['SECRET_KEY'] = '591356516325e2cf84f935336e974b3b5a354af26e67c0e2'
song_data = pd.read_csv('data/RealDataset.csv')
input_songs = []

@app.route('/', methods=('GET', 'POST'))
def index():
    playlist_form = PlaylistForm()
    formatted_songs = format_songs()

    if request.method == 'POST':
        action = request.form['action']

        if action == 'submit_song':
            title = request.form['title']
            title_split = title.split(" - ")
            input_songs.insert(0, {'track_name': title_split[0], 'artist_name': title_split[1]})

            if 'add_another' in request.form:
                return render_template('index.html', songs=input_songs, song_list=formatted_songs,
                                       playlist_form=playlist_form)

            elif 'submit' in request.form:
                input_songs_df = pd.DataFrame(input_songs, columns = ['track_name', 'artist_name'])
                songs = multiple_song_recommender(song_data, input_songs_df)
                song_list = songs.values.tolist()
                return render_template('results.html', songs=song_list, song_list=formatted_songs,
                                       playlist_form=playlist_form)

        if action == 'submit_playlist':
            if playlist_form.validate_on_submit():
                playlist_link = request.form['playlist_link']
                playlist_df = playlist_extractor(playlist_link)
                songs = multiple_song_recommender(song_data, playlist_df)
                song_list = songs.values.tolist()

                return render_template('results.html', songs=song_list)

    return render_template('index.html', songs=input_songs,
                           playlist_form=playlist_form, song_list=formatted_songs)


def format_songs():
    songs_list = list(zip(song_data.track_name, song_data.artist_name))
    new_songs = []
    for song in songs_list:
        new_songs.append(str(song[0] + " - " + song[1]))
    new_songs = list(dict.fromkeys(new_songs))
    new_songs = sorted(new_songs)
    return new_songs


@app.route('/del_song', methods=('GET', 'POST'))
def delete_song():
    title = request.args.get('title')
    artist = request.args.get('artist')
    input_songs.remove({'track_name':title, 'artist_name':artist})
    return redirect(url_for('index'))


@app.route('/messages/<int:idx>')
def message(idx):
    messages = ['Message Zero', 'Message One', 'Message Two']
    try:
        return render_template('messages.html', message=messages[idx])
    except IndexError:
        abort(404)


@app.errorhandler(404)
def page_not_found(error):
    return render_template('404.html'), 404






