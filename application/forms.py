from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, SelectField
from wtforms.validators import *
import pandas as pd
import spotipy
from spotipy.oauth2 import SpotifyClientCredentials

#spotify stuff
cid = "350dc6e606a04d3abad5816f98f615f8"
secret = "9d61d1cfab874a51a8cedcefe406dc0c"
client_credentials_manager = SpotifyClientCredentials(client_id=cid, client_secret=secret)
sp = spotipy.Spotify(client_credentials_manager=client_credentials_manager)


class PlaylistForm(FlaskForm):
    playlist_link = StringField(
        'Playlist Link',
        [DataRequired()]
    )

    def validate_playlist_link(form, playlist_link):
        playlist_uri = playlist_link.data.split("/")[-1].split("?")[0]
        try:
            track_uris = [x["track"]["uri"] for x in sp.playlist_tracks(playlist_uri)["items"]]
        except spotipy.SpotifyException:
            raise ValidationError("Oops! That isn't a valid playlist!")



