import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity
import spotipy
from spotipy.oauth2 import SpotifyClientCredentials

#spotify stuff
cid = "350dc6e606a04d3abad5816f98f615f8"
secret = "9d61d1cfab874a51a8cedcefe406dc0c"
client_credentials_manager = SpotifyClientCredentials(client_id=cid, client_secret=secret)
sp = spotipy.Spotify(client_credentials_manager=client_credentials_manager)

#import dataset
songs_df = pd.read_csv('data/RealDataset.csv')

cv = CountVectorizer()
asm = cv.fit_transform(['artist_name'])
cosine_sim = cosine_similarity(asm, asm)

def playlist_extractor(playlist_link):
    """playlist_extractor converts a Spotify playlist link to a dataframe of song title and artist
    :param playlist_link: the link of the Spotify playlist
    :return: a dataframe of song titles and their artists
    """
    #get the playlist uri from the link
    playlist_uri = playlist_link.split("/")[-1].split("?")[0]
    #lists to hold the song and artist names
    songs = []
    artists = []

    ##loop through each item in the playlist and get the song and artist name for each
    for item in sp.playlist_tracks(playlist_uri)["items"]:
        song_name = item["track"]["name"]
        artist_name = item["track"]["artists"][0]["name"]
        #add each one to the respective list
        songs.append(song_name)
        artists.append(artist_name)

    #combine the lists and convert them into a dataframe
    playlist_songs_df = pd.DataFrame(list(zip(songs, artists)), columns = ["track_name", "artist_name"])

    return playlist_songs_df
    
    
def multiple_song_recommender(data, playlist):
    """multiple_song_recommender generates recommendations for multiple songs
    :param data: the songs csv
    :param playlist: the dataframe of songs to be used for the recommendation
    :return: a dataframe containing the top 5 songs recommended songs, based on similarity
    """
    # find the songs that were entered in the features dataset
    features_of_playlist = data[data['artist_name'].isin(playlist['artist_name'].values)]
    features_of_playlist = features_of_playlist[features_of_playlist['track_name'].isin(playlist['track_name'].values)]

    #get the entire dataset minus the songs that were entered because we don't want to recommend them
    non_playlist_audio_features = data[~data['track_name'].isin(playlist['track_name'].values)]
    
    #remove non-comparable values and put the remaining in a single vector
    final_list = features_of_playlist.drop(columns = ["artist_name", "track_name", "track_id", "duration_ms", "time_signature", ])
    single_vector = final_list.sum(axis = 0)
    
    #generate the recommendations
    recom_playlist = generate_recoms(data, single_vector, non_playlist_audio_features).head(5)
    
    return recom_playlist[["artist_name", "track_name", "track_id"]]
    

def generate_recoms(data, single_vector, df_not_in_playlist):
    """generate_recoms is the main function that uses cosine similarity to generate recommendations
    :param data: the songs csv
    :param single_vector: the resulting vector from combining the values for all the songs in a playlist
    :param df_not_in_playlist: entire songs dataframe minus the ones that were in the input dataframe
    :return:
    """
    non_playlist_songs_df = data[data['track_name'].isin(df_not_in_playlist['track_name'].values)]
    #find the cosine similarity score between the summarized vector (that represents the songs in the playlist) and each of the songs in the dataset
    non_playlist_songs_df['score'] = cosine_similarity(df_not_in_playlist.drop(columns = ["artist_name", "track_name", "track_id", "duration_ms", "time_signature"]).values,
                                               single_vector.values.reshape(1, -1))[:,0]
    #sort them in descending order by the score
    non_playlist_songs_df = non_playlist_songs_df.sort_values(by="score", ascending = False)
    
    return non_playlist_songs_df